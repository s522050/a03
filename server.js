var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var server = require('http').createServer(app); // inject app into the server

// Setting view paths
app.use(express.static(__dirname + '/assets'));
app.set("view engine", "ejs") // specify our view engine
app.set("views", path.resolve(__dirname, "views")) // path to views


// 2 create an array to manage our entries
var entries = []
app.locals.entries = entries // now entries can be accessed in .ejs files

// 3 set up an http request logger to log every request automagically
app.use(logger("dev"))     // app.use() establishes middleware functions
app.use(bodyParser.urlencoded({ extended: false }))

// Handle http GET requests 
app.get("/", function (request, response) {
    response.sendFile(__dirname+"/views/homepage.html")
})
app.get("/homepage", function (request, response) {
    response.sendFile(__dirname+"/views/homepage.html");
});
app.get("/Tip-calculator", function (request, response) {
    response.sendFile(__dirname+"/views/Tip-calculator.html");
});
app.get("/contact", function (request, response) {
    response.sendFile(__dirname+"/views/contact.html");
});
app.get("/new-entry", function (request, response) {
    response.render("new-entry")
})
app.get("/guestbook", function (request, response) {
    response.render("index")
})


// Handle POST requests 
app.post("/new-entry", function (request, response) {
    if (!request.body.title || !request.body.body) {
        response.status(400).send("Entries must have a title and a body.")
        return
    }
    entries.push({  // store it
        title: request.body.title,
        content: request.body.body,
        published: new Date()
    })
    response.redirect("/guestbook")  // where to go next? Let's go to the home page :)
})

app.post("/contact", function (request, response) {
    var api_key = 'key-78308a443a5279e1f63dd1b2a0c27c7f';
    var domain = 'sandbox4f07809077754589af4215003d775205.mailgun.org';
    var mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });

    var data = {
        from: 'Mail Gun rjsvkfdl91 <postmaster@sandbox4f07809077754589af4215003d775205.mailgun.org>',
        to: 's522050@nwmissouri.edu',
        subject: request.body.title,
        text: request.body.message
    };

    mailgun.messages().send(data, function (error, body) {
        console.log(body);
        if (!error)
            response.send("Mail sent successfully!");
        else
            response.send("Failed to send email!");        
    });
});

// if we get a 404 status, render our 404.ejs view
app.use(function (request, response) {
    response.status(404).render("404")
})

// Listen for an application request on port 8081 & notify the developer
//http.listen(8081, function () {
// console.log('Guestbook app listening on http://127.0.0.1:8081/')
//})

// set up the view engine
// manage our entries
// set up the logger
// GETS
// POSTS
// 404

// Listen for an application request on port 8081
server.listen(8081, function () {
    console.log('Guestbook app listening on http://127.0.0.1:8081/');
});